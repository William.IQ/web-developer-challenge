import { NgModule } from '@angular/core';

// Module Imports
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { MomentModule } from 'ngx-moment';

// Component Imports
import { AppComponent } from './app.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { HeaderComponent } from './components/header/header.component';
import { OverviewComponent } from './components/overview/overview.component';
import { OverviewPanelComponent } from './components/overview/overview-panel/overview-panel.component';
import { BulkPaymentsComponent } from './components/bulk-payments/bulk-payments.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HelpButtonComponent } from './components/help-button/help-button.component';

@NgModule({
  declarations: [
    AppComponent,
    SideBarComponent,
    HeaderComponent,
    OverviewComponent,
    OverviewPanelComponent,
    BulkPaymentsComponent,
    NotFoundComponent,
    HelpButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    NgbModule,
    FormsModule,
    ChartsModule,
    MomentModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
