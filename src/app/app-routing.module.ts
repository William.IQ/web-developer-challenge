import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverviewComponent } from './components/overview/overview.component';
import { BulkPaymentsComponent } from './components/bulk-payments/bulk-payments.component';
import { NotFoundComponent } from './components/not-found/not-found.component';


const routes: Routes = [
  { path: 'overview', component: OverviewComponent },
  { path: 'bulk-payments', component: BulkPaymentsComponent },
  { path: '',   redirectTo: '/overview?environment=sandbox', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
