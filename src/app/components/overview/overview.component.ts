import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  public chartData = {
    labels: [
      'Jun 1',
      'Jun 2',
      'Jun 3',
      'Jun 4',
      'Jun 5',
      'Jun 6',
      'Jun 7',
      'Jun 8',
      'Jun 9',
      'Jun 10',
      'Jun 11',
      'Jun 12',
      'Jun 13',
      'Jun 14',
      'Jun 15',
      'Jun 16',
      'Jun 17',
      'Jun 18',
      'Jun 19',
      'Jun 20',
      'Jun 21',
      'Jun 22',
      'Jun 23',
      'Jun 24',
      'Jun 25',
      'Jun 26',
      'Jun 27',
      'Jun 28',
      'Jun 29',
      'Jun 30',
      'Jul 1',
    ],
    data: [
      35000,
      20000,
      35000,
      38000,
      12000,
      35000,
      35000,
      20000,
      35000,
      38000,
      12000,
      35000,
      20000,
      35000,
      38000,
      12000,
      35000,
      35000,
      20000,
      35000,
      38000,
      12000,
      35000,
      38000,
      12000,
      35000,
      20000,
      35000,
      38000,
      12000,
      35000,
      35000
    ]
  };

  public availableBalance = 560987.00;
  public volumeBalance = 34050.00;
  public accountBalance = 1999999.99;
  public settleAmount = 398009.42;
  public revenueAmount = 12670.90;
  public ordersAmount = 28;

  constructor() { }

  ngOnInit(): void {
  }

}
