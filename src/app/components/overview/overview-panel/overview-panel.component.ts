import { Component, OnInit, Input } from '@angular/core';
import { faDownload, faLongArrowAltRight } from '@fortawesome/free-solid-svg-icons';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-overview-panel',
  templateUrl: './overview-panel.component.html',
  styleUrls: ['./overview-panel.component.scss']
})
export class OverviewPanelComponent implements OnInit {

  public download = faDownload;
  public arrowRight = faLongArrowAltRight;
  public live = false;
  public updateDate: Date;
  @Input() public panelName: string;
  @Input() public activeRange: string;
  @Input() public currentMetric = 'Total volume';
  @Input() public currentCurrency: string;
  @Input() public useMetrics: boolean;
  @Input() public balance: string;
  @Input() public bottomSectionLeftTitle: string;
  @Input() public bottomsectionLeftData: string;
  @Input() public bottomSectionRightTitle: string;
  @Input() public bottomSectionRightData: string;

  @Input() private ChartType: string;
  @Input() private ChartData: number[];
  @Input() private ChartLabels: string[];

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        ticks: { fontColor: 'rgba(147, 121, 171, 0.98)' },
        gridLines: { color: 'transparent' }
      }],
      yAxes: [{
        ticks: { fontColor: '#9b9b9b' },
        gridLines: { color: '#d6d6d6' }
      }]
    }
  };
  public chartLegend = false;
  public chartLabels: Label[];
  public chartType = 'bar';
  public chartData = [{
    data: [],
    backgroundColor: '#aed3ff',
    borderColor: '#aed3ff',
    pointBorderColor: '#603b80',
    pointBackgroundColor: '#fbfbfb',
    pointRadius: 6,
  }];

  constructor() {
    this.updateData();
  }

  ngOnInit(): void {
    this.chartLabels = this.ChartLabels;
    this.chartData[0].data = this.ChartData;
    this.chartType = this.ChartType;
    setInterval(() => {
      this.updateData();
    }, 300000);
  }

  updateData(): void {
    this.updateDate = new Date();
  }

}
