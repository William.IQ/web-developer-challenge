import { Component, OnInit } from '@angular/core';
import { faCommentDots } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-help-button',
  templateUrl: './help-button.component.html',
  styleUrls: ['./help-button.component.scss']
})
export class HelpButtonComponent implements OnInit {

  help = faCommentDots;

  constructor() { }

  ngOnInit(): void {
  }

}
