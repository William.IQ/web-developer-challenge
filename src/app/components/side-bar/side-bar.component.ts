import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  faCheck,
  faPoll,
  faCartPlus,
  faShoppingBag,
  faCreditCard,
  faShoppingCart,
  faLink,
  faQrcode,
  faFire,
  faSlidersH,
  faCalendarDay,
  faClock,
  faCog,
  IconDefinition,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {

  public icons = {
    activate: faCheck,
    overview: faPoll,
    transactions: faCartPlus,
    settlement: faShoppingBag,
    paymentSettings: faCreditCard,
    createPayment: faShoppingCart,
    paymentLinks: faLink,
    bulkPayments: faCartPlus,
    myDevices: faQrcode,
    monitoring: faFire,
    apiKeys: faSlidersH,
    events: faCalendarDay,
    logs: faClock,
    settings: faCog
  };

  public environment = 'sandbox';


  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      const environment = params['environment'];
      this.environment = environment;
    });
  }

}
