import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { faQuestionCircle, faBell, faUser } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  faQuestionCircle = faQuestionCircle;
  faBell = faBell;
  faUser = faUser;

  public environment = 'sandbox';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      const environment = params['environment'];
      this.environment = environment;
    });
  }

  changeEnvironment(environment: string): void {
    const route = this.router.url.split('?')[0];
    this.router.navigate([route], {queryParams: {environment}});
  }

}
