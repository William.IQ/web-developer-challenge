import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  faInfoCircle,
  faLongArrowAltRight,
  faChevronLeft,
  faChevronRight,
  faSearch
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-bulk-payments',
  templateUrl: './bulk-payments.component.html',
  styleUrls: ['./bulk-payments.component.scss']
})
export class BulkPaymentsComponent implements OnInit {

  public icons = {
    infoIcon: faInfoCircle,
    arrowRight: faLongArrowAltRight,
    chevronLeft: faChevronLeft,
    chevronRight: faChevronRight,
    search: faSearch
  };
  public environment = 'sandbox';

  public activeRange: string;
  public tableData = [];

  constructor(private activatedRoute: ActivatedRoute) {
    this.activeRange = 'today';
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      const environment = params['environment'];
      this.environment = environment;
    });
  }

  public handleFileInput(files: FileList) {
    console.log(`File to Upload: ${files.item(0).name}`);
  }

}
